# Ynov Pipeline Intégration
_Charlie HAUSELMANN / Anthony GUICHARD_

PHP 7.3 - Mysql (latest) - API Platform (Symfony 5.1) - GitLab - SonarCloud

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=code_smells)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=coverage)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)

[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=sqale_index)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=alert_status)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=charlie.hauselmann_ynov-pipeline-integration&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=charlie.hauselmann_ynov-pipeline-integration)

Mise en place d'une Pipeline d'intégration pour API Platform avec GitLab et SonarQube.

### Objectif

Cette Pipeline a pour objectif de proposer un déploiement continu pour une application Symfony API Platform.

L'objectif est de disposer de ces différentes étapes :
- un Linter pour les erreurs de syntaxes
- un Phase de Build qui construit une image Docker
- une phase de Test 
- une analyse de la qualité du code via SonarCloud
- une phase de déploiement (à implémenter)

### Architecture

Le squelette de l'application se trouve dans le sous dossier de l'application, c'est une API Platform Symfony. 

La pipeline se base sur un Dockerfile qui créé une image basée sur PHP 7.3.

On y ajoute plusieurs outils tel que :
- PHP Unit (pour les tests)
- Xdebug (Pour les rapports de couverture des tests)
- Composer qui est nécessaire pour Symfony
- PDO Mysql

Cette image embarque également le code de l'application.

### Workflow

Lors du push du code sur Gitlab, la pipeline va en premier temps analyser la syntaxe du code.

Dans un second temps l'image docker de l'application est générée et le projet est embarqué dedans.

Ensuite les tests sont lancés via PHP Unit sur cette dernière. Cette étape va générer les rapports de tests ainsi que le coverage de l'application.

On réalise ensuite une analyse via SonarCloud pour tester la qualité du code.

Enfin la dernière étape consiste à déployer l'application. 
Cette dernière étape n'est pas configurée et doit donc l'être en fonction des besoins. 

## Configuration

les fichiers de configuration sont les suivants :
- Dockerfile : Pour la création de l'image Docker
- .gitlab-ci.yml : Pour l'édition de la pipeline
- sonar-project.properties : Pour la configuration de SonarCloud
- phpunit.xml.dist : Pour la configuration de PHPUnit

# Utilisation

### Build
Dans un premier temps il faut cloner le projet. On peut ensuite créer l'image docker à partir du fichier docker-compose.yml avec la commande suivante :
``` shell script
docker-compose up --build -d
```

Ce fichier va créer une image docker à partir des images suivantes:
- php
- mysql
- nginx

On peut ensuite acceder au site à l'adresse localhost:8080/public 

Le swagger se trouve sur l'url localhost:8080/public/index.php/api

Le contenu de l'application se trouve dans le dossier demo-api-platform et notamment le dossier src.

### Tests
Pour la création des tests il est possible de créer une classe ExempleTest.php dans le dossier tests qui sera exécutée lors du lancement de la pipeline ou avec la commande : 
```
phpunit
```

Il est possible de configurer phpunit en éditant le fichier phpunit.xml.dist

Les fichiers de rapport sont générés dans le dossier build/reports.

### SonarCloud
Il est nécessaire de créer un compte sur [SonarCloud](https://sonarcloud.io/), de suivre la procédure de configuration et d'éditer le fichier sonar-project.properties à la racine du projet.
```
sonar.projectKey=charlie.hauselmann_ynov-pipeline-integration
sonar.organization=charlie-hauselmann

 # Statically scanned directory, unit test directory
sonar.sources=demo-api-platform/src
sonar.tests=demo-api-platform/tests

 # Specify code coverage report file, unit test result file
sonar.php.coverage.reportPath=demo-api-platform/build/reports/phpunit.coverage.xml
sonar.php.tests.reportPath=demo-api-platform/build/reports/phpunit.xml
```
