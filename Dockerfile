FROM php:7.3-apache

RUN apt-get update -y \
  && apt-get install -y git zip unzip \
  && apt-get update -yqq \
  && apt-get install git -yqq \
  && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
  && curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar \
  && chmod +x /usr/local/bin/phpunit \
  && pecl install xdebug redis \
  && docker-php-ext-enable xdebug redis \
  && docker-php-source delete \
  && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && echo "xdebug.remote_port=9001" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && echo "xdebug.idekey=mertblog.net" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && echo "xdebug.remote_host=docker.for.mac.localhost" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
  && docker-php-ext-install pdo pdo_mysql

COPY . /src

WORKDIR /src/demo-api-platform

RUN /usr/local/bin/composer install --no-scripts

EXPOSE 8000

CMD [ "apache2ctl", "-D", "FOREGROUND" ]
